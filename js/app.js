function cargarAxio() {
    const url = "/html/alumnos.json"
    const promedio = document.getElementById("promedio")
    var prom
    var promge = 0

    axios
        .get(url)
        .then((res) => {
            mostrar(res.data)
        })
        .catch((error) => {
            console.log("surgio un error" + error)
        })

    function mostrar(data) {
        const res = document.getElementById('tablaResultado')
        res.innerHTML = ""
        for (item of data) {
            prom = (item.matematicas + item.quimica + item.fisica + item.geografia) / 4 
            promge = promge + prom 
            res.innerHTML += `
                <tr>
					<td>${item.id}</td>
					<td>${item.matricula}</td>
					<td>${item.nombre}</td>
					<td>${item.matematicas}</td>
                    <td>${item.quimica}</td>
					<td>${item.fisica}</td>
					<td>${item.geografia}</td>
					<td>${prom}</td>
				</tr>`;
            
        }

        promedio.innerHTML = `${promge / 18}`
    }
}

document.getElementById("btnMostrar").addEventListener("click", function () {
    cargarAxio();
})

document.getElementById("btnLimpiar").addEventListener("click", function () {
    const res = document.getElementById("respuesta")
    res.innerHTML = `
    <table id="respuesta" class="table table-bordered table-striped">
        <thead class="">
            <th>Id</th>
            <th>Matricula</th>
            <th>Nombre</th>
            <th>Matematicas</th>
            <th>Quimica</th>
            <th>Fisica</th>
            <th>Geografia</th>
            <th>Promedio</th>
        </thead>
        <tbody id="tablaResultado">

        </tbody>
    </table>`
    promedio.innerHTML=""

})